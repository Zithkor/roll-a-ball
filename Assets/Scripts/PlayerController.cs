﻿
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{

    public float speed;
    public Text countText;
    public Text winText;
    public GameObject player;
    public Text timeRemainingDisplayText;
    public Button restart;
    public Button quit;

    private float timeRemaining;
    private Rigidbody rb;
    private int count;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;
        SetCountText();
        winText.text = "";
        timeRemaining = 20;
        UpdateTimeRemainingDisplay();
        restart.gameObject.SetActive(false);
        Button btn = restart.GetComponent<Button>();
        btn.onClick.AddListener(TaskOnClick);
        quit.gameObject.SetActive(false);
        Button btn2 = quit.GetComponent<Button>();
        btn2.onClick.AddListener(TaskOnClick1);
        timeRemainingDisplayText.color = Color.white;
    }

    private void UpdateTimeRemainingDisplay()
    {
        timeRemainingDisplayText.text = "Time: " + Mathf.Round(timeRemaining).ToString();
    }

    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        rb.AddForce(movement * speed);

        

        if (timeRemaining > 0)
        {
            timeRemaining -= Time.deltaTime;
            UpdateTimeRemainingDisplay();

            if (timeRemaining <= 5)
            {
                timeRemainingDisplayText.color = Color.red;
            }
        }
        else
        {
            winText.text = "You Lose!";
            player.SetActive(false);
            restart.gameObject.SetActive(true);
            quit.gameObject.SetActive(true);
        }
    }


    void TaskOnClick()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    
    void TaskOnClick1()
    {
       Application.Quit();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pick Up"))
        {
            other.gameObject.SetActive(false);
            count = count + 1;
            SetCountText();
        }
    }

    void SetCountText()
    {
        countText.text = "Count: " + count.ToString();
        if (count >= 13)
        {
            winText.text = "You Win!";
            player.SetActive(false);
            restart.gameObject.SetActive(true);
            quit.gameObject.SetActive(true);
        }
    }
}